//
//  MortTextField.m
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import "MortTextField.h"

@implementation MortTextField
{
    NSNumberFormatter *numberFormatter;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (void) initialise
{
    if (_nonCurrencyField == NO)
    {
        self.delegate = self;
    }
    
    self.keyboardType = UIKeyboardTypeNumberPad;
    
    numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:0];
    [numberFormatter setMinimumFractionDigits:0];
    
    UIToolbar *numberToolbar = [self setUpNumberToolbar];
    self.inputAccessoryView = numberToolbar;
    
    //self.clearButtonMode = UITextFieldViewModeWhileEditing;
}

- (UIToolbar *)setUpNumberToolbar
{
    NSString *finalButtonText = _isFinalTextBox ? @"Done" : @"Next";
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    
    
    if (!_isFinalTextBox)
    {
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                               [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneNumberPad)],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:finalButtonText style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadNext)],
                               nil];

    }
    else
    {
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               [[UIBarButtonItem alloc]initWithTitle:finalButtonText style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadNext)],
                               nil];
    }
    [numberToolbar sizeToFit];
    return numberToolbar;
}

- (void)awakeFromNib
{
    [self initialise];
}

- (float) getNumericalValue
{
    NSMutableString *selfText = [NSMutableString stringWithString:self.text];
    [self removeSymbols:selfText];
    
    return selfText.floatValue;
}

-(void) setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"nonCurrencyField"])
    {
        BOOL valueAsNumber = value;
        _nonCurrencyField = valueAsNumber;
    }
    
    if ([key isEqualToString:@"isFinalTextBox"])
    {
        BOOL valueAsNumber = value;
        _isFinalTextBox = valueAsNumber;
    }
}

- (void)removeSymbols:(NSMutableString *)textFieldTextStr
{
    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.currencySymbol
                                      withString:@""
                                         options:NSLiteralSearch
                                           range:NSMakeRange(0, [textFieldTextStr length])];
    
    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                      withString:@""
                                         options:NSLiteralSearch
                                           range:NSMakeRange(0, [textFieldTextStr length])];
    /*
    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.decimalSeparator
                                      withString:@""
                                         options:NSLiteralSearch
                                           range:NSMakeRange(0, [textFieldTextStr length])];
     */
}

- (NSString *)formatStringAsCurrency:(NSString *)textFieldTextStrNonMutable
{
    NSMutableString *textFieldTextStr = [NSMutableString stringWithString:textFieldTextStrNonMutable];
    [self removeSymbols:textFieldTextStr];
    
    NSDecimalNumber *textFieldTextNum = [NSDecimalNumber decimalNumberWithString:textFieldTextStr];
    
    if ([textFieldTextNum isEqual:[NSDecimalNumber notANumber]])
        return @"";
    
    NSDecimalNumber *divideByNum = [[[NSDecimalNumber alloc] initWithInt:10] decimalNumberByRaisingToPower:numberFormatter.maximumFractionDigits];
    NSDecimalNumber *textFieldTextNewNum = [textFieldTextNum decimalNumberByDividingBy:divideByNum];
    NSString *textFieldTextNewStr = [numberFormatter stringFromNumber:textFieldTextNewNum];
    return textFieldTextNewStr;
}

- (void) reformatTextBoxAsCurrency
{
    self.text = [self formatStringAsCurrency:self.text];
}

- (BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string
{
    NSInteger MAX_DIGITS = 11; // $999,999,999.99
    
    NSString *stringMaybeChanged = [NSString stringWithString:string];
    if (stringMaybeChanged.length > 1)
    {
        NSMutableString *stringPasted = [NSMutableString stringWithString:stringMaybeChanged];
        
        [stringPasted replaceOccurrencesOfString:numberFormatter.currencySymbol
                                      withString:@""
                                         options:NSLiteralSearch
                                           range:NSMakeRange(0, [stringPasted length])];
        
        [stringPasted replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                      withString:@""
                                         options:NSLiteralSearch
                                           range:NSMakeRange(0, [stringPasted length])];
        
        NSDecimalNumber *numberPasted = [NSDecimalNumber decimalNumberWithString:stringPasted];
        stringMaybeChanged = [numberFormatter stringFromNumber:numberPasted];
    }
    
    UITextRange *selectedRange = [textField selectedTextRange];
    UITextPosition *start = textField.beginningOfDocument;
    NSInteger cursorOffset = [textField offsetFromPosition:start toPosition:selectedRange.start];
    NSMutableString *textFieldTextStr = [NSMutableString stringWithString:textField.text];
    NSUInteger textFieldTextStrLength = textFieldTextStr.length;
    
    [textFieldTextStr replaceCharactersInRange:range withString:stringMaybeChanged];
    
    if (textFieldTextStr.length <= MAX_DIGITS && textFieldTextStr.length > 0)
    {
        NSString *textFieldTextNewStr;
        textFieldTextNewStr = [self formatStringAsCurrency:textFieldTextStr];
        
        textField.text = textFieldTextNewStr;
        
        if (cursorOffset != textFieldTextStrLength)
        {
            NSInteger lengthDelta = textFieldTextNewStr.length - textFieldTextStrLength;
            NSInteger newCursorOffset = MAX(0, MIN(textFieldTextNewStr.length, cursorOffset + lengthDelta));
            UITextPosition* newPosition = [textField positionFromPosition:textField.beginningOfDocument offset:newCursorOffset];
            UITextRange* newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
            [textField setSelectedTextRange:newRange];
        }
    }
    
    if ([textFieldTextStr  isEqual: @""])
    {
        textField.text = @"";
        return YES;
    }
    
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.text = [self formatStringAsCurrency:textField.text];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) doneNumberPad
{
    [self resignFirstResponder];
}

-(void)cancelNumberPad
{
    [self resignFirstResponder];
    self.text = @"";
}

-(void)doneWithNumberPadNext
{
    [self.nextTextField becomeFirstResponder];
    [self resignFirstResponder];
}

@end
