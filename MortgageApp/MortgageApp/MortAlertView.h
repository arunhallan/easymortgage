//
//  MortAlertView.h
//  MortgageApp
//
//  Created by Arun Hallan on 09/10/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MortAlertView : UIAlertView

@property (strong, nonatomic) NSString *typeChanged;

@end
