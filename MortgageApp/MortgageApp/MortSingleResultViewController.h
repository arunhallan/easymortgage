//
//  MortSingleResultViewController.h
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MortTextField.h"
#import "MortResultInfo.h"
#import <iAd/iAd.h>

@interface MortSingleResultViewController : UITableViewController<UITextFieldDelegate, ADBannerViewDelegate>

@property (strong, nonatomic) MortResultInfo *mortgageInfo;
@property (weak, nonatomic) IBOutlet MortTextField *totalTextField;
@property (weak, nonatomic) IBOutlet UISlider *totalSlider;

@property (weak, nonatomic) IBOutlet MortTextField *borrowTextField;
@property (weak, nonatomic) IBOutlet UISlider *borrowSlider;

@property (weak, nonatomic) IBOutlet MortTextField *depositTextField;
@property (weak, nonatomic) IBOutlet UISlider *depositSlider;

@property (weak, nonatomic) IBOutlet MortTextField *irTextField;
@property (weak, nonatomic) IBOutlet UISlider *irSlider;

@property (weak, nonatomic) IBOutlet MortTextField *termTextField;
@property (weak, nonatomic) IBOutlet UISlider *termSlider;

@property (weak, nonatomic) IBOutlet MortTextField *monthlyPaymentsTextField;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@end
