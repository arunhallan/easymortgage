//
//  MortResultInfo.h
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MortResultInfo : NSObject

@property (strong, nonatomic) NSNumber *totalValue;
@property (strong, nonatomic) NSNumber *monthlyMortgageRate;
@property (strong, nonatomic) NSNumber *depositValue;
@property (strong, nonatomic) NSNumber *borrowValue;
@property (strong, nonatomic) NSNumber *interestRateValue;
@property (strong, nonatomic) NSNumber *termInYears;
@property (nonatomic) BOOL *interestOnly;

@end
