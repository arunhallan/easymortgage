//
//  MortCalculator.m
//  MortgageApp
//
//  Created by Arun Hallan on 09/10/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import "MortCalculator.h"

@implementation MortCalculator

- (float) calculateMonthlyMortgageRate:(float) borrowed
                                  rate:(float) rate
                             termYears:(float) termYears
                          interestOnly:(BOOL) interestOnly
{
    float rateMonthly = (rate/100.0) / 12.0;
    float months = termYears * 12;
    
    if (interestOnly)
    {
        return (rateMonthly * borrowed);
    }
    
    float rateCompoundedByMonthsNumerator = powf(1 + rateMonthly, months) - 1;
    float rateCompoundedByMonthsDivisor = powf(1 + rateMonthly, months)  * rateMonthly;
    
    float discountFactor = rateCompoundedByMonthsNumerator/rateCompoundedByMonthsDivisor;
    
    float costMonthly = borrowed / discountFactor;
    
    return costMonthly;
}

- (float) calculateBorrowed:(float) totalPerMonth
                       rate:(float) rate
                  termYears:(float) termYears
               interestOnly:(BOOL) interestOnly
{
    float rateMonthly = (rate/100.0) / 12.0;
    float months = termYears * 12;
    
    if (interestOnly)
    {
        return totalPerMonth/rateMonthly;
    }
    
    float rateCompoundedByMonthsNumerator = powf(1 + rateMonthly, months) - 1;
    float rateCompoundedByMonthsDivisor = powf(1 + rateMonthly, months)  * rateMonthly;
    
    float discountFactor = rateCompoundedByMonthsNumerator/rateCompoundedByMonthsDivisor;
    
    return totalPerMonth * discountFactor;
}

- (float) calculateInterestRate:(float) totalPerMonth
                       borrowed:(float)borrowed
                  termYears:(float) termYears
               interestOnly:(BOOL) interestOnly
{
    // slow method until we can find a better methodology
    float increment = 0.25;
    float current = 0.25;
    BOOL found = NO;
    
    while (current < 50)
    {
        float monthly = [self calculateMonthlyMortgageRate:borrowed rate:current termYears:termYears interestOnly:interestOnly];
        if (monthly > totalPerMonth)
        {
            found = YES;
            break;
        }
        
        current = current + increment;
    }
    
    if (!found)
        return 0.0;
    
    return current;
}

- (float) calculateTermYears:(float) totalPerMonth
                       borrowed:(float)borrowed
                           rate:(float) rate
                   interestOnly:(BOOL) interestOnly
{
    float rateMonthly = (rate/100.0) / 12.0;
    
    if (interestOnly)
    {
        return 25.0;
    }
    
    float loggedNumerator = logf(1.0 / (1 - (borrowed/totalPerMonth) * rateMonthly));
    float loggedDivisor = logf(1 + rateMonthly);

    float months = loggedNumerator/loggedDivisor;
    
    return months/12.0;
}

@end
