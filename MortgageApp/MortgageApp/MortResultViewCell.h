//
//  MortResultViewCell.h
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MortResultViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthlyPaymentLabel;

@end
