//
//  MortBorrowMainTableViewController.h
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MortTextField.h"

@interface MortBorrowMainTableViewController : UITableViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet MortTextField *borrowTextField;
@property (weak, nonatomic) IBOutlet MortTextField *depositTextField;
@property (weak, nonatomic) IBOutlet MortTextField *totalTextField;
@property (weak, nonatomic) IBOutlet MortTextField *irTextField;
@property (weak, nonatomic) IBOutlet MortTextField *termTextField;
@property (weak, nonatomic) IBOutlet MortTextField *depositPercentageField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end
