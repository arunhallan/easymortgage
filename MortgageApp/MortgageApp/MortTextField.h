//
//  MortTextField.h
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MortTextField : UITextField<UITextFieldDelegate>

- (float) getNumericalValue;
- (void) reformatTextBoxAsCurrency;

@property (nonatomic) BOOL nonCurrencyField;
@property (nonatomic) BOOL isFinalTextBox;
@property (strong, nonatomic) MortTextField* nextTextField;

@end
