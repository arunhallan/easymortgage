//
//  MortCalculator.h
//  MortgageApp
//
//  Created by Arun Hallan on 09/10/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MortCalculator : NSObject

- (float) calculateMonthlyMortgageRate:(float) borrowed
                                  rate:(float) rate
                             termYears:(float) termYears
                          interestOnly:(BOOL) interestOnly;

- (float) calculateBorrowed:(float) totalPerMonth
                       rate:(float) rate
                  termYears:(float) termYears
               interestOnly:(BOOL) interestOnly;

- (float) calculateInterestRate:(float) totalPerMonth
                       borrowed:(float)borrowed
                      termYears:(float) termYears
                   interestOnly:(BOOL) interestOnly;

- (float) calculateTermYears:(float) totalPerMonth
                    borrowed:(float)borrowed
                        rate:(float) rate
                interestOnly:(BOOL) interestOnly;

@end
