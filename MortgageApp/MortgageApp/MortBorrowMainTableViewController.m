//
//  MortBorrowMainTableViewController.m
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import "MortBorrowMainTableViewController.h"
#import "MortResultInfo.h"
#import "MortSingleResultViewController.h"

@interface MortBorrowMainTableViewController ()

@end

@implementation MortBorrowMainTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Annoyingly sometimes there's a bug so have to set manually
    _irTextField.nonCurrencyField = YES;
    _termTextField.nonCurrencyField = YES;
    
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    // Scroll up a bit
    self.tableView.contentOffset = CGPointMake(0, 20);
    
    _irTextField.keyboardType = UIKeyboardTypeDecimalPad;
    
    _totalTextField.nextTextField = _depositTextField;
    _depositTextField.nextTextField = _depositPercentageField;
    _depositPercentageField.nextTextField = _borrowTextField;
    _borrowTextField.nextTextField = _irTextField;
    _irTextField.nextTextField = _termTextField;
}

-(void)cancelNumberPad
{
    [_totalTextField resignFirstResponder];
    _totalTextField.text = @"";
}

-(void)doneWithNumberPad
{
    [_totalTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)idDidEndEditing:(id)sender
{
    [self textFieldDidEndEditing:sender];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString* restorationId = textField.restorationIdentifier;
    
    if ([restorationId  isEqual: @"borrowText"])
    {
        [self recalcDepositFromTotalAndBorrow];
        [self recalcDepositPercentage];
    }
    
    if ([restorationId  isEqual: @"totalText"])
    {
        [self recalcBorrowFromTotalAndDeposit];
    }
    
    if ([restorationId  isEqual: @"depositPercentageField"])
    {
        float depositPercentage = [_depositPercentageField getNumericalValue];
        float totalValue= [_totalTextField getNumericalValue];
        float newDepositValue = totalValue * (depositPercentage/100.0);
        float newBorrowValue = totalValue - newDepositValue;
        
        NSNumber* newDepositNumber = [NSNumber numberWithFloat:newDepositValue];
        _depositTextField.text = [newDepositNumber stringValue];
        _borrowTextField.text = [[NSNumber numberWithFloat:newBorrowValue] stringValue];
    }
    
    if ([restorationId  isEqual: @"depositText"])
    {
        [self recalcBorrowFromTotalAndDeposit];
        [self recalcDepositPercentage];
    }
    
    [_depositTextField reformatTextBoxAsCurrency];
    [_borrowTextField reformatTextBoxAsCurrency];
    [_totalTextField reformatTextBoxAsCurrency];
}

- (void) recalcDepositPercentage
{
    float deposit = [_depositTextField getNumericalValue];
    float totalValue = [_totalTextField getNumericalValue];
    
    float newDepositPercent = (deposit/totalValue)*100.0;
    
    if (!isnan(newDepositPercent) && newDepositPercent != 0)
    {
        NSNumber* newDepositNumber = [NSNumber numberWithFloat:newDepositPercent];
        _depositPercentageField.text = [newDepositNumber stringValue];
    }
    else
    {
        _depositPercentageField.text = @"";
    }
}

- (void) recalcDepositFromTotalAndBorrow
{
    float borrowValue = [_borrowTextField getNumericalValue];
    float totalValue = [_totalTextField getNumericalValue];
    float depositValue = totalValue - borrowValue;
    NSNumber* depositValueNumber = [NSNumber numberWithFloat:depositValue];
    _depositTextField.text = [depositValueNumber stringValue];
}

- (void) recalcBorrowFromTotalAndDeposit
{
    float totalValue = [_totalTextField getNumericalValue];
    float depositValue = [_depositTextField getNumericalValue];
    float borrowValue = totalValue - depositValue;
    NSNumber* borrowNumber = [NSNumber numberWithFloat:borrowValue];
    _borrowTextField.text = [borrowNumber stringValue];
}

- (BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSString *blank = @"";
    
    if ([_totalTextField.text isEqualToString:blank])
    {
        [self showUIAlertView:@"House Price"];
        return NO;
    }
    
    if ([_depositTextField.text isEqualToString:blank])
    {
        [self showUIAlertView:@"Deposit"];
        return NO;
    }
    
    if ([_borrowTextField.text isEqualToString:blank])
    {
        [self showUIAlertView:@"Borrow"];
        return NO;
    }
    
    if ([_irTextField.text isEqualToString:blank])
    {
        [self showUIAlertView:@"Interest Rate"];
        return NO;
    }
    
    if ([_termTextField.text isEqualToString:blank])
    {
        [self showUIAlertView:@"Term"];
        return NO;
    }
    
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    MortResultInfo *resultInfo = [[MortResultInfo alloc]init];
    resultInfo.borrowValue = [NSNumber numberWithFloat:[_borrowTextField getNumericalValue]];
    resultInfo.depositValue = [NSNumber numberWithFloat:[_depositTextField getNumericalValue]];
    resultInfo.interestRateValue = [NSNumber numberWithFloat:_irTextField.text.floatValue];
    resultInfo.termInYears = [NSNumber numberWithFloat:_termTextField.text.floatValue];
    resultInfo.totalValue = [NSNumber numberWithFloat:[_totalTextField getNumericalValue]];
    resultInfo.interestOnly = (_segmentedControl.selectedSegmentIndex == 1);
    
    MortSingleResultViewController *resultTableController = [segue destinationViewController];
    resultTableController.mortgageInfo = resultInfo;
}

- (void) showUIAlertView:(NSString*) message
{
    NSString *fullMessage = [NSString stringWithFormat:@"Please complete the %@ field", message];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:fullMessage
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
