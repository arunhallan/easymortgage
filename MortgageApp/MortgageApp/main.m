//
//  main.m
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MortAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MortAppDelegate class]));
    }
}
