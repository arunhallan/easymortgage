//
//  MortSingleResultViewController.m
//  MortgageApp
//
//  Created by Arun Hallan on 11/09/2014.
//  Copyright (c) 2014 Arun. All rights reserved.
//

#import "MortSingleResultViewController.h"
#import "MortCalculator.h"
#import "MortAlertView.h"

@interface MortSingleResultViewController ()

@end

@implementation MortSingleResultViewController
{
    MortCalculator *mortCalculator;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mortCalculator = [[MortCalculator alloc]init];
    
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    // Scroll up a bit
    self.tableView.contentOffset = CGPointMake(0, 20);
    
    _totalTextField.text = [_mortgageInfo.totalValue stringValue];
    _borrowTextField.text = [_mortgageInfo.borrowValue stringValue];
    _depositTextField.text = [_mortgageInfo.depositValue stringValue];
    _irTextField.text = [_mortgageInfo.interestRateValue stringValue];
    _termTextField.text = [_mortgageInfo.termInYears stringValue];
    _segmentedControl.selectedSegmentIndex = _mortgageInfo.interestOnly ? 1 : 0;
    
    [self initialiseSlider:_totalSlider sliderValue:_mortgageInfo.totalValue.floatValue sliderValueMax:_mortgageInfo.totalValue.floatValue * 1.25];
    [self initialiseSlider:_depositSlider sliderValue:_mortgageInfo.depositValue.floatValue sliderValueMax:_mortgageInfo.totalValue.floatValue];
    [self initialiseSlider:_borrowSlider sliderValue:_mortgageInfo.borrowValue.floatValue sliderValueMax:_mortgageInfo.totalValue.floatValue];
    [self initialiseSlider:_irSlider sliderValue:_mortgageInfo.interestRateValue.floatValue sliderValueMax:20];
    [self initialiseSlider:_termSlider sliderValue:_mortgageInfo.termInYears.floatValue sliderValueMax:50];
    
    [self setMonthlyPaymentsTextFieldValue];
    
    [_totalTextField reformatTextBoxAsCurrency];
    [_borrowTextField reformatTextBoxAsCurrency];
    [_depositTextField reformatTextBoxAsCurrency];
    [_monthlyPaymentsTextField reformatTextBoxAsCurrency];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initialiseSlider:(UISlider*) slider sliderValue:(float)sliderValue sliderValueMax:(float)sliderValueMax
{
    [self initialiseSlider:slider sliderValue:sliderValue sliderValueMax:sliderValueMax sliderValueMin:0];
}

- (void) initialiseSlider:(UISlider*) slider sliderValue:(float)sliderValue sliderValueMax:(float)sliderValueMax sliderValueMin:(float)sliderValueMin
{
    slider.maximumValue = sliderValueMax;
    slider.minimumValue = sliderValueMin;
    slider.value = sliderValue;
    [slider setUserInteractionEnabled:YES];
}

- (void)setBorrowSliderBounds
{
    _borrowSlider.maximumValue = _totalSlider.value;
    _borrowSlider.minimumValue = 0;
}

- (void) setDepositBounds
{
    _depositSlider.maximumValue = _totalSlider.value;
    _depositSlider.minimumValue = 0;
}

- (int) getGenericRoundedValue:(UISlider*) slider
{
    return [self roundNumber:slider.value roundIncrement:10000];
}

- (float) roundNumber:(float) number roundIncrement:(float)roundIncrement
{
    float halfIncrement = roundIncrement / 2.0;
    return ((int)((number + halfIncrement) / roundIncrement) * roundIncrement);
}

- (IBAction)sliderValueChanged:(id)sender
{
    [self sliderValueChangedRound:sender roundValues:YES];
}

- (void) sliderValueChangedRound:(UISlider*)slider roundValues:(BOOL) roundValues
{
    if ([slider.restorationIdentifier  isEqual: @"totalSlider"])
    {
        if (roundValues)
            [slider setValue:[self getGenericRoundedValue:slider] animated:NO];
        
        _totalTextField.text = [NSNumber numberWithFloat:_totalSlider.value].stringValue;
        
        [self setBorrowSliderBounds];
        [self setDepositBounds];
        
        _borrowSlider.value = _totalSlider.value - _depositSlider.value;
        _borrowTextField.text = [NSNumber numberWithFloat:_borrowSlider.value].stringValue;
    }
    
    if ([slider.restorationIdentifier  isEqual: @"borrowSlider"])
    {
        if (roundValues)
            [slider setValue:[self getGenericRoundedValue:slider] animated:NO];
        
        _depositSlider.value = _totalSlider.value - _borrowSlider.value;
        _borrowTextField.text = [NSNumber numberWithFloat:_borrowSlider.value].stringValue;
        _depositTextField.text = [NSNumber numberWithFloat:_depositSlider.value].stringValue;
    }
    
    if ([slider.restorationIdentifier  isEqual: @"depositSlider"])
    {
        if (roundValues)
            [slider setValue:[self getGenericRoundedValue:slider] animated:NO];
        
        _borrowSlider.value = _totalSlider.value - _depositSlider.value;
        _depositTextField.text = [NSNumber numberWithFloat:_depositSlider.value].stringValue;
        _borrowTextField.text = [NSNumber numberWithFloat:_borrowSlider.value].stringValue;
    }
    
    if ([slider.restorationIdentifier  isEqual: @"irSlider"])
    {
        float increment = 0.25;
        
        if (roundValues)
            [slider setValue:[self roundNumber:slider.value roundIncrement:increment] animated:NO];
        
        _irTextField.text = [NSNumber numberWithFloat:_irSlider.value].stringValue;
    }
    
    if ([slider.restorationIdentifier  isEqual: @"termSlider"])
    {
        float increment = 1;
        
        if (roundValues)
            [slider setValue:[self roundNumber:slider.value roundIncrement:increment] animated:NO];
        
        _termTextField.text = [NSNumber numberWithFloat:_termSlider.value].stringValue;
    }
    
    [self setMonthlyPaymentsTextFieldValue];
    [_totalTextField reformatTextBoxAsCurrency];
    [_borrowTextField reformatTextBoxAsCurrency];
    [_depositTextField reformatTextBoxAsCurrency];
    [_monthlyPaymentsTextField reformatTextBoxAsCurrency];
}

- (IBAction)touchDragExit:(id)sender
{
    UISlider *slider = sender;
    
    if ([slider.restorationIdentifier  isEqual: @"totalSlider"])
    {
        slider.value = [_totalTextField getNumericalValue];
    }
    
    if ([slider.restorationIdentifier  isEqual: @"borrowSlider"])
    {
        slider.value = [_borrowTextField getNumericalValue];
    }
    
    if ([slider.restorationIdentifier  isEqual: @"depositSlider"])
    {
        slider.value = [_depositTextField getNumericalValue];
    }
    
    if ([slider.restorationIdentifier  isEqual: @"irSlider"])
    {
        slider.value = [_irTextField getNumericalValue];
    }
    
    if ([slider.restorationIdentifier  isEqual: @"termSlider"])
    {
        slider.value = [_termTextField getNumericalValue];
    }
}

- (void) setMonthlyPaymentsTextFieldValue
{
    BOOL interestOnly = _segmentedControl.selectedSegmentIndex == 1;
    
    float monthlyPayments = [mortCalculator calculateMonthlyMortgageRate:_borrowSlider.value rate:_irSlider.value termYears:_termSlider.value interestOnly:interestOnly];
    
    _monthlyPaymentsTextField.text = [NSString stringWithFormat:@"%.f", monthlyPayments];
}

- (IBAction)segmentChanged:(id)sender
{
    [self setMonthlyPaymentsTextFieldValue];
    [_monthlyPaymentsTextField reformatTextBoxAsCurrency];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    banner.hidden = YES;
    NSLog(@"bannerview did not receive any banner due to %@", error);
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    NSLog(@"bannerview was selected");
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    banner.hidden = NO;
    NSLog(@"banner was loaded");
}

- (IBAction)valueButtonPressed:(id)sender
{
    UIView *aView = sender;
    
    NSString *fullMessage = [NSString stringWithFormat:@"Enter your desired monthly amount to calculate the %@ needed", aView.restorationIdentifier];
    
    MortAlertView * alert = [[MortAlertView alloc]
                           initWithTitle:@"How much do you want to pay a month?"
                           message:fullMessage
                           delegate:self
                           cancelButtonTitle:@"Cancel"
                           otherButtonTitles:@"OK", nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.typeChanged = aView.restorationIdentifier;
    
    UITextField* tf = [alert textFieldAtIndex:0];
    tf.keyboardType = UIKeyboardTypeDecimalPad;
    
    [alert show];
}


- (void)alertView:(MortAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        float desiredValue = [alertView textFieldAtIndex:0].text.floatValue;
        if (desiredValue == 0)
            return;
        
        [self setDesiredValueAndSlidersAfterGoalSeek:desiredValue valueToChangeType:alertView.typeChanged];
    }
}

- (void) setDesiredValueAndSlidersAfterGoalSeek:(float) desiredValue valueToChangeType:(NSString*) valueToChangeType
{
    BOOL interestOnly = _segmentedControl.selectedSegmentIndex == 1;
    
    if ([valueToChangeType  isEqualToString: @"Total"] ||
        [valueToChangeType  isEqualToString: @"Borrow"] ||
        [valueToChangeType  isEqualToString: @"Deposit"])
    {
        float borrowed = [mortCalculator calculateBorrowed:desiredValue rate:_irSlider.value termYears:_termSlider.value interestOnly:interestOnly];
        
        if ([valueToChangeType  isEqualToString: @"Total"])
        {
            _totalSlider.value = borrowed + _depositSlider.value;
            [self sliderValueChangedRound:_totalSlider roundValues:NO];
        }
        
        if ([valueToChangeType  isEqualToString: @"Borrow"])
        {
            _borrowSlider.value = borrowed;
            [self sliderValueChangedRound:_borrowSlider roundValues:NO];
        }
        
        if ([valueToChangeType  isEqualToString: @"Deposit"])
        {
            _depositSlider.value = _totalSlider.value - borrowed;
            [self sliderValueChangedRound:_depositSlider roundValues:NO];
        }
        
        _monthlyPaymentsTextField.text = [NSString stringWithFormat:@"%.f", desiredValue];
        [_monthlyPaymentsTextField reformatTextBoxAsCurrency];
    }
    
    if ([valueToChangeType  isEqualToString: @"I.R."])
    {
        float interestRate = [mortCalculator calculateInterestRate:desiredValue borrowed:_borrowSlider.value termYears:_termSlider.value interestOnly:interestOnly];
        _irSlider.value = interestRate;
        [self sliderValueChangedRound:_irSlider roundValues:NO];
        
        float actualValue = [mortCalculator calculateMonthlyMortgageRate:_borrowSlider.value rate:interestRate termYears:_termSlider.value interestOnly:interestOnly];
        _monthlyPaymentsTextField.text = [NSString stringWithFormat:@"%.f", actualValue];
        [_monthlyPaymentsTextField reformatTextBoxAsCurrency];
    }
    
    if ([valueToChangeType isEqualToString:@"Term"])
    {
        float term = [mortCalculator calculateTermYears:desiredValue borrowed:_borrowSlider.value rate:_irSlider.value interestOnly:interestOnly];
        float termRounded = [self roundNumber:term roundIncrement:0.1];
        _termSlider.value = termRounded;
        [self sliderValueChangedRound:_termSlider roundValues:NO];
        
        float actualValue = [mortCalculator calculateMonthlyMortgageRate:_borrowSlider.value rate:_irSlider.value termYears:termRounded interestOnly:interestOnly];
        _monthlyPaymentsTextField.text = [NSString stringWithFormat:@"%.f", actualValue];
        [_monthlyPaymentsTextField reformatTextBoxAsCurrency];
    }
    
}

@end
